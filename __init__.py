##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2021 Carlos Scotta <saludpublica@ingenieria.uner.edu.ar>
#    Copyright (C) 2021 Ernesto Ridel <saludpublica@ingenieria.uner.edu.ar>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.pool import Pool
from .health_dentistry import *
from .report import odontology_statistics_report
from .report import professional_ministry_report
from .report import odontogram_CORA_report
from .wizard import create_odontology_reports
from .wizard import create_ministry_report
from .wizard import help_activate_temporal
from .wizard import help_load_treatments
from .wizard import wizard_appointment_dentistry_treatment


def register():
    Pool.register(
        health_dentistry.PatientData,
        health_dentistry.DentistryTreatment,
        create_odontology_reports.CreateOdontologyReportsStart,
        create_ministry_report.CreateMinistryReportStart,
        help_activate_temporal.HelpActivateTemporalStart,
        help_load_treatments.HelpLoadTreatmentsStart,
        module='health_dentistry_fiuner', type_='model')
    Pool.register(
        create_odontology_reports.CreateOdontologyReportsWizard,
        help_activate_temporal.HelpActivateTemporalWizard,
        help_load_treatments.HelpLoadTreatmentsWizard,
        create_ministry_report.CreateMinistryReportWizard,
        wizard_appointment_dentistry_treatment.OpenAppointmentDentistryTreatment,
        module='health_dentistry_fiuner', type_='wizard')
    Pool.register(
        odontology_statistics_report.OdontologyStatisticsReport,
        professional_ministry_report.MinistryByProfessionalReport,
        odontogram_CORA_report.OdontogramCORA,
        module='health_dentistry_fiuner', type_='report')
