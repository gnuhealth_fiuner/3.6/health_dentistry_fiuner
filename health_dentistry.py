##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2021 Carlos Scotta <saludpublica@ingenieria.uner.edu.ar>
#    Copyright (C) 2021 Ernesto Ridel <saludpublica@ingenieria.uner.edu.ar>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import json
from datetime import date
from collections import defaultdict

from trytond.model import fields, Unique
from trytond.pyson import Eval, Equal
from trytond.pool import Pool, PoolMeta


__all__ = [ 'PatientData','DentistryTreatment']

class PatientData(metaclass=PoolMeta):
    'Patient Data'
    __name__ = 'gnuhealth.patient'
    
    @staticmethod
    def default_use_primary_schema():
        return True


class DentistryTreatment(metaclass=PoolMeta):
    'Dentistry Treatment'
    __name__ = 'gnuhealth.dentistry.treatment'

    STATES = {'readonly': Eval('state') == 'done'}
    first_time = fields.Boolean('First Time', states=STATES)
    urgency = fields.Boolean('Urgency', states=STATES)
    attendance = fields.Boolean('Attendance', states=STATES)
    discharge = fields.Selection([
        (None, ''),
        ('basic', 'Basic Discharge'),
        ('middle', 'Middle Discharge'),
        ('integral', 'Integral Discharge'),
        ], 'Odontologic Discharge', sort=False, states=STATES)

    @staticmethod
    def default_first_time():
        return False
    
    @staticmethod
    def default_urgency():
        return False
    
    @staticmethod
    def default_attendance():
        return False
    
    @staticmethod
    def default_discharge():
        return ''
