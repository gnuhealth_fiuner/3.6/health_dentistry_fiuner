# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

__all__ = ['OdontologyStatisticsReport']

class OdontologyStatisticsReport(Report):
    'Odontology Report'
    __name__ = 'gnuhealth.dentistry.reports'
    
    
    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        Profesional = pool.get('gnuhealth.healthprofessional')
        Treatments = pool.get('gnuhealth.dentistry.treatment')
        Procedures = pool.get('gnuhealth.dentistry.procedure')
        
        context = super(OdontologyStatisticsReport, cls).get_context(records, data) #es un diccionario
        
        if data:
            start = data['start']
            end = data['end']
            start_d = start.date()
            end_d = end.date()

        
            appointment_check = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('state','=','checked_in'),
                                ('speciality','=','Odontología'),])
        
            appointment_done = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('state','=','done'),
                                ('speciality','=','Odontología'),])
        
            appointment_no_show = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('state','=','no_show'),
                                ('speciality','=','Odontología'),])
            
            appointment_insurance = Appointments.search([
                                ('appointment_date','>=',start),
                                ('appointment_date','<=',end),
                                ('patient.current_insurance','=','True'),
                                ('speciality','=','Odontología'),])
            
            profesionales = Profesional.search([('main_specialty','=','Odontología'),])
            
            treatments = Treatments.search([
                                ('treatment_date','>=',start_d),
                                ('treatment_date','<=',end_d),
                                ])
            
            procedures = Procedures.search([])
            
            context['start_date'] = start
            context['end_date'] = end
            context['current_date'] = date.today()
        
        
            #Rangos Etarios Femeninos con turno
            context['f_0'] =0
            context['f_1_4'] =0
            context['f_5_9'] =0
            context['f_10_14'] =0
            context['f_15_19'] =0
            context['f_20_24'] =0
            context['f_25_39'] =0
            context['f_40_64'] =0
            context['f_65_plus'] =0
            
            context['f_0'] =len ([x for x in appointment_check if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.name.gender == 'f'])
            
            context['f_1_4'] =len ([x for x in appointment_check if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.name.gender == 'f'])
            
            context['f_5_9'] =len ([x for x in appointment_check if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.name.gender == 'f'])
            
            context['f_10_14'] =len ([x for x in appointment_check if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.name.gender == 'f'])
            
            context['f_20_24'] =len ([x for x in appointment_check if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.name.gender == 'f'])
            
            context['f_25_39'] =len ([x for x in appointment_check if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.name.gender == 'f'])
            
            context['f_40_64'] =len ([x for x in appointment_check if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.name.gender == 'f'])
            
            context['40-64-f'] =len ([x for x in appointment_check if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.name.gender == 'f'])
            
            context['f_65_plus'] =len ([x for x in appointment_check if (x.patient.age_float >= 65) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 65 ) and x.patient.name.gender == 'f']) 
            
            
            #Rangos Etarios Masculinos con turno
            context['m_0'] =0
            context['m_1_4'] =0
            context['m_5_9'] =0
            context['m_10_14'] =0
            context['m_15_19'] =0
            context['m_20_24'] =0
            context['m_25_39'] =0
            context['m_40_64'] =0
            context['m_65_plus'] =0
            
            context['m_0'] =len ([x for x in appointment_check if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.name.gender == 'm'])
            
            context['m_1_4'] =len ([x for x in appointment_check if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.name.gender == 'm'])
            
            context['m_5_9'] =len ([x for x in appointment_check if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.name.gender == 'm'])
            
            context['m_10_14'] =len ([x for x in appointment_check if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.name.gender == 'm'])
            
            context['m_15_19'] =len ([x for x in appointment_check if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.name.gender == 'm'])
            
            context['m_20_24'] =len ([x for x in appointment_check if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.name.gender == 'm'])
            
            context['m_25_39'] =len ([x for x in appointment_check if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.name.gender == 'm'])
            
            context['m_40_64'] =len ([x for x in appointment_check if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.name.gender == 'm'])
            
            context['m_65_plus'] =len ([x for x in appointment_check if (x.patient.age_float >= 65) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 65 ) and x.patient.name.gender == 'm']) 
            
            #Totales Ausentes
            context['no_show_total']=0
            
            context['no_show_total'] = len ([x for x in appointment_no_show if (x.patient.age_float >= 0 and x.patient.age_float < 120)])
            
            
            #Totales por grupos etarios 
            context['m_f_0'] = context['m_0'] + context['f_0']
            context['m_f_1_4'] =context['m_1_4'] + context['f_1_4']
            context['m_f_5_9'] =context['m_5_9'] + context['f_5_9']
            context['m_f_10_14'] =context['m_10_14'] + context['f_10_14']
            context['m_f_15_19'] =context['m_15_19'] + context['f_15_19']
            context['m_f_20_24'] =context['m_20_24'] + context['f_20_24']
            context['m_f_25_39'] =context['m_25_39'] + context['f_25_39']
            context['m_f_40_64'] =context['m_40_64'] + context['f_40_64']
            context['m_f_65_plus'] = context['m_65_plus'] + context['f_65_plus']
            
            #Totales Presentes por Sexo
            context['total_f'] =0
            context['total_m'] =0
            
            context['total_f'] = len ([x for x in appointment_check if (x.patient.age_float >= 0 and x.patient.age_float < 120) and x.patient.name.gender == 'f']) + len ([x for x in appointment_done if (x.patient.age_float >= 0 and x.patient.age_float < 120) and x.patient.name.gender == 'f'])
            
            context['total_m'] = len ([x for x in appointment_check if (x.patient.age_float >= 0 and x.patient.age_float < 120) and x.patient.name.gender == 'm']) + len ([x for x in appointment_done if (x.patient.age_float >= 0 and x.patient.age_float < 120) and x.patient.name.gender == 'm'])
            
            context['total_m_f'] = context['total_m'] + context['total_f'] 
            
            #Pacientes con Obra Social
            context['os_0']=0
            context['os_1_4']=0
            context['os_5_9']=0
            context['os_10_14']=0
            context['os_15_19']=0
            context['os_20_24']=0
            context['os_25_39']=0
            context['os_40_64']=0
            context['os_65_plus']=0
            context['os_total']=0
            
            context['os_0'] = len([x for x in appointment_check if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 0 and x.patient.age_float < 1) and x.patient.current_insurance])
            
            context['os_1_4'] = len([x for x in appointment_check if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 1 and x.patient.age_float < 5) and x.patient.current_insurance])
            
            context['os_5_9'] = len([x for x in appointment_check if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 5 and x.patient.age_float < 10) and x.patient.current_insurance])
            
            context['os_10_14'] = len([x for x in appointment_check if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 10 and x.patient.age_float < 15) and x.patient.current_insurance])
            
            context['os_15_19'] = len([x for x in appointment_check if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 15 and x.patient.age_float < 20) and x.patient.current_insurance])
            
            context['os_20_24'] = len([x for x in appointment_check if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 20 and x.patient.age_float < 25) and x.patient.current_insurance])
            
            context['os_25_39'] = len([x for x in appointment_check if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 25 and x.patient.age_float < 40) and x.patient.current_insurance])
            
            context['os_40_64'] = len([x for x in appointment_check if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 40 and x.patient.age_float < 65) and x.patient.current_insurance])
            
            context['os_65_plus'] = len([x for x in appointment_check if (x.patient.age_float >= 65) and x.patient.current_insurance]) + len([x for x in appointment_done if (x.patient.age_float >= 65) and x.patient.current_insurance])
            
            context['os_total']=len([x for x in appointment_check if (x.patient.age_float >= 0)]) + len([x for x in appointment_done if (x.patient.age_float >= 0 ) and x.patient.current_insurance])
            
            
            #Ausentes
            
            context['no_show'] = [0,0,0,0,0,0,0,0,0]
            
            for i in appointment_no_show:
            	if i.patient.name.gender == 'm' or i.patient.name.gender == 'f':
            		if i.patient.age_float < 1:
            			context['no_show'][0] +=1
            		elif i.patient.age_float >= 1 and i.patient.age_float < 5:
            			context['no_show'][1] +=1
            		elif i.patient.age_float >= 5 and i.patient.age_float < 10:
            			context['no_show'][2] +=1
            		elif i.patient.age_float >= 10 and i.patient.age_float < 15:
            			context['no_show'][3] +=1
            		elif i.patient.age_float >= 15 and i.patient.age_float < 20:
            			context['no_show'][4] +=1
            		elif i.patient.age_float >= 20 and i.patient.age_float < 25:
            			context['no_show'][5] +=1
            		elif i.patient.age_float >= 25 and i.patient.age_float < 40:
            			context['no_show'][6] +=1	
            		elif i.patient.age_float >= 40 and i.patient.age_float < 65:
            			context['no_show'][7] +=1
            		elif i.patient.age_float >= 65:
            			context['no_show'][8] +=1
            
              

            
            # Obtengo los profesionles y las atenciones de cada uno 	
            context['prof_total'] = {} 
            context['prof'] = []
           
            for x in profesionales:
            	prof = x.name.lastname + ', ' + x.name.name 
 
            	if prof in context['prof']:
            		pass
            	else:
            		context['prof'].append(prof)		
            		context['prof_total'][prof] = 0

            for x in appointment_check:
            	prof = x.healthprof.name.lastname + ', ' + x.healthprof.name.name 
            	context['prof_total'][prof] += 1
            
            for x in appointment_done:
            	prof = x.healthprof.name.lastname + ', ' + x.healthprof.name.name 
            	context['prof_total'][prof] += 1
            
            #Para trabajar con los datos Booleanos de los tratamientos Odontológicos
            
            context['first_time'] = 0
            context['urgency'] = 0
            context['attendance'] = 0
            context['basic_discharge'] = 0
            context['middle_discharge'] = 0
            context['integral_discharge'] = 0            
            
            for x in treatments:
                if x.first_time==True:
                    context['first_time'] +=1
                
            for x in treatments:
                if x.urgency==True:
                    context['urgency'] +=1
            
            for x in treatments:
                if x.attendance==True:
                    context['attendance'] +=1
           
            for x in treatments:
                if x.discharge == 'basic':
                    context['basic_discharge'] +=1
                elif x.discharge == 'middle':
                    context['middle_discharge'] +=1
                elif x.discharge == 'integral':
                    context['integral_discharge'] +=1
            
            #Para trabajar con los Procedimientos Odontológicos
            
            context['total_proc_masc'] = { }
            context['total_proc_fem'] = {}
            context['total_proc'] = {}
            
            context['name_proc']={}
            context['codes'] = []
            
            for x in procedures:
                context['total_proc_masc'][x.code]= 0
                context['total_proc_fem'][x.code]= 0
                context['name_proc'][x.code]= x.name 
                context['codes'].append(x.code)
            
            context['total_proc_masc']['TOTAL']= 0
            context['total_proc_fem']['TOTAL']= 0
            
            for i in treatments:
                aux=i.procedures
                for j in aux:
                    if i.patient.name.gender =='f':
                        context['total_proc_fem'][j.procedure.code] += 1
                        context['total_proc_fem']['TOTAL'] += 1
                    elif i.patient.name.gender =='m':
                        context['total_proc_masc'][j.procedure.code] += 1
                        context['total_proc_masc']['TOTAL'] += 1

                   
                                 
        return context






