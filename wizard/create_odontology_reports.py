from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields

__all__ = ['CreateOdontologyReportsStart', 'CreateOdontologyReportsWizard']


class CreateOdontologyReportsStart(ModelView):    
    'Odontology Report Start'
    __name__ = 'gnuhealth.dentistry.reports.start'
    
    report_ = fields.Selection([
        ('create_odontology_report', 'Odontology Statistics'),
         ],'Odontolgy Reports',required=True,sort=False)
    start_date = fields.DateTime('Start Date', required=True)
    end_date = fields.DateTime('End Date', required=True)
    
    @staticmethod
    def default_report_():
        return 'create_odontology_report'


class CreateOdontologyReportsWizard(Wizard):
    'Odontology Report Wizard'
    __name__ = 'gnuhealth.dentistry.reports.wizard'
    
    @classmethod
    def __setup__(cls):
        super(CreateOdontologyReportsWizard,cls).__setup__()
        cls._error_messages.update({
            'end_date_before_start_date': 'The end date cannot be major thant the start date',
            })
    
    start = StateView('gnuhealth.dentistry.reports.start',
                      'health_dentistry_fiuner.create_odontology_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    create_odontology_report =\
        StateAction('health_dentistry_fiuner.act_gnuhealth_odontology_report')

    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            self.raise_user_error('end_date_before_start_date')
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date

        return {
            'start':start, 
            'end':end
            }

    def do_create_odontology_report(self, action):
        data = self.fill_data()
        return action, data
