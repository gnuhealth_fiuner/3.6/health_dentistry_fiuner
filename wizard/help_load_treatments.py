from trytond.wizard import Wizard, StateView, Button
from trytond.model import ModelView, fields


__all__ = ['HelpLoadTreatmentsStart', 'HelpLoadTreatmentsWizard']


class HelpLoadTreatmentsStart(ModelView):    
    'Help for load Treatments'
    __name__ = 'gnuhealth.dentistry.help_load_treatments.start'
    
    text = fields.Text('How to load treatments',
                       readonly=True)
    
    @staticmethod
    def default_text():
        return ("1- Buscar el Paciente en la lista de pacientes\n"
            "2- Seleccionar el paciente o ingresar a la ficha del mismo\n"
            "3- Desde la barra de acciones (donde están los íconos) seleccionar 'Relacionado'''\n"
            "4- Hacer click en 'Tratamientos Odontológicos'\n"
            "5- Se abre una ventana con todos los tratamientos odontológicos del paciente seleccionado\n"
            "6- Hacer click en 'Nuevo' en la barra de acciones\n"
            "7- Ya podemos Registrar Procedimientos o modificar el odontograma del Paciente: \n"
            "8-         Registrar Procedimientos ->  Botón 'Cargar Procedimientos' \n"
            "9-         Modificar el odontograma -> Botón 'Modificar Odontograma' \n")


class HelpLoadTreatmentsWizard(Wizard):
    'Help for load Treatments Wizard'
    __name__ = 'gnuhealth.dentistry.help_load_treatments.wizard'
    
    start = StateView('gnuhealth.dentistry.help_load_treatments.start',
                      'health_dentistry_fiuner.help_load_treatments_start_view',[
                        Button('Close','end','tryton-cancel', default=True),
                        ])
